import { useBlockProps, InnerBlocks } from '@wordpress/block-editor';
import './style.scss'

export default function saveBlock( { attributes } ) {
    const blockProps = useBlockProps.save();
    const {
		loop,
		autoplay,
		navigation,
		pagination
	} = attributes
    
    return (
        <div {...blockProps}>
            <swiper-container 
                slides-per-view="1" 
                navigation={navigation} 
                pagination={pagination} 
                loop={loop} 
                autoplay={autoplay}
                scrollbar="false"
            >
                <InnerBlocks.Content />
            </swiper-container>
        </div>
    );
    // return (
    //     <div {...blockProps}>
    //         <div className='container row swiper'>
    //             <div className='swiper-wrapper'>
    //                 <InnerBlocks.Content />
    //             </div>
    //             <div className='swipper-button-next'></div>
    //             <div className='swipper-button-pagination'></div>
    //         </div>
    //     </div>
    // );
}
