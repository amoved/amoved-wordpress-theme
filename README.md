# amoved WordPress theme 

## Descripción
Tema de bloques para la web de [amoved](https://amoved.es) basado en el editor de bloques de Gutenberg de WordPress dockerizado para el entorno de desarrollo en local.

<img alt="Montaje de la vista 'Hacemos'" src="./awt/screenshot.png" height=300>

## Entorno de desarrollo
Para poder añadir modificaciones al código y testearlo en local utiliza la imagen de docker provista, para ello:

```bash
cd amoved-wordpress-theme
docker-compose up
```

**Nota:** cuando realices cambios en las plantillas del tema desde el editor visual de WordPress tendrás restaurar el estado inicial de la misma si haces cambios en el HTML.  

## Contacto
Para cualquier duda acerca del tema contacta con el equipo.

## Autoría
Desarrollado por Luis H. Porras y Carlos Escudero desde [amoved](https://amoved.es)

## Licencia
Este código se encuentra licencia bajo la licencia [GPLv3](https://www.gnu.org/licenses/gpl-3.0.en.html)
