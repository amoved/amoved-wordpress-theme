import {
    InnerBlocks,
	useBlockProps
} from '@wordpress/block-editor';
import './editor.scss'

const ALLOWED_BLOCKS = [ 
	'core/paragraph', 
	'core/image',
	'core/blocks',
	'core/columns', 
	'awt/rich-text' 
];

export default function editBlock( { } ) {
    
    return (
        <>
            <div {...useBlockProps()}>
				<InnerBlocks allowedBlocks={ALLOWED_BLOCKS} />
			</div>
        </>
    );
	// return (
    //     <>
    //         <div className='swiper-slide'>
	// 			<InnerBlocks allowedBlocks={ALLOWED_BLOCKS} />
	// 		</div>
    //     </>
    // );
}