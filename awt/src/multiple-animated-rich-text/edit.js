import { 
    useBlockProps, 
    InnerBlocks, 
} from '@wordpress/block-editor';
import { Placeholder } from '@wordpress/components';
import { loop as icon } from '@wordpress/icons';
import { useSelect } from '@wordpress/data';
import { __ } from '@wordpress/i18n';
import './editor.scss'



export default function editBlock( { isSelected, clientId } ) {
    
	const is_inner_block_selected = useSelect(
		( select ) => select( 'core/block-editor' ).hasSelectedInnerBlock( clientId )
	);
	
    if ( isSelected || is_inner_block_selected ) {
		return (
			<div { ...useBlockProps() }>
				<Placeholder
					// className="block-editor-media-placeholder"
					// withIllustration={ true }
					icon={ icon }
					label={ __( 'Efecto de escritura para uno o varios títulos. ' ) }
					instructions={ __(
						'Añade uno o varios bloques de título animado. El efecto se irá aplicando de forma que los distintos títulos irán apareciendo en orden, en bucle.\n\n Nota: en el editor de WordPress solo verás el primer título animarse cuando pinches fuera de este bloque, guarda y previsualiza la página para ver el efecto completo.'
					) }
				>
	                <InnerBlocks allowedBlocks={["awt/animated-rich-text"]}/>
				</Placeholder>
			</div>
		);
	}

    return (
		<div { ...useBlockProps({className: 'is-not-selected'}) }>
			<div className='block-inner row'>
				<div className='col'>
					<InnerBlocks />
				</div>
			</div>
		</div>
    );
}