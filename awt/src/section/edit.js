import { 
    useBlockProps, 
    InnerBlocks, 
} from '@wordpress/block-editor';
import './editor.scss'

const ALLOWED_BLOCKS = [
    "awt/rich-text",
    "awt/multiple-animated-rich-text",
    "awt/section-carrousel",
    "awt/dropdown-card",
    "core/block", 
    "core/paragraph", 
    "core/columns"
]
export default function editBlock() {
    
    return (
        <>
            <section { ...useBlockProps() }>
                <div className='container'>
					<InnerBlocks allowedBlocks={ALLOWED_BLOCKS}/>
				</div>
            </section>
        </>
    );
}