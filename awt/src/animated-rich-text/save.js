/**
 * External dependencies
 */
import classnames from 'classnames';

/**
 * WordPress dependencies
 */
import { RichText, useBlockProps } from '@wordpress/block-editor';

import './style.scss'

export default function save( { attributes } ) {
	const { animationSteps, textAlign, content, level } = attributes;
	const TagName = 'h' + level;

	const className = classnames( {
		[ `has-text-align-${ textAlign }` ]: textAlign,
	} );

	const style = {
		animation: `animated-text ${animationSteps*.1}s steps(${animationSteps},end) 1s infinite,
		animated-cursor .8s ease-in-out infinite`
	}

	return (
		<div className='wp-block-awt-animated-rich-text-container'>
			<TagName { ...useBlockProps.save( { className, style } ) }>
				<RichText.Content value={ content } />
			</TagName>
		</div>
	);
}
