<?php

function add_theme_supports() {

	// Add supports
	add_theme_support( 'wp-block-styles' );
	
	// Add editor assets
	add_editor_style( './assets/css/views/index.css' );

}
add_action( 'after_setup_theme', 'add_theme_supports' );

function add_theme_scripts() {

	// Enqueue general purpose styles
	wp_enqueue_style('swiper-style',  get_theme_file_uri( "lib/swiper/swiper-bundle.min.css" ) );
	wp_enqueue_style('fork-awesome-style',  get_theme_file_uri( "lib/fork-awesome/css/fork-awesome.min.css" ) );
	// wp_enqueue_style('style',  get_theme_file_uri( "assets/css/style.css" ) );
	wp_enqueue_style('awt-style',  get_theme_file_uri( "assets/css/build/awt.css" ) );
	wp_enqueue_style('user-blocks-style',  get_theme_file_uri( "assets/css/build/user-blocks.css" ) );
	wp_enqueue_style('views-style',  get_theme_file_uri( "assets/css/build/views.css" ) );

	// Enqueue scripts
	wp_enqueue_script( 'swiper-script', get_template_directory_uri() . '/lib/swiper/swiper-element.min.js', '', 1.1, true );
	wp_enqueue_script( 'main-script', get_template_directory_uri() . '/assets/js/main.js', '', 1.1, true );
	wp_enqueue_script( 'mart-script', get_template_directory_uri() . '/assets/js/multiple-animated-rich-text.js', '', 1.1, true );

	// Front-end index page global styles
	if(is_page('hacemos')){
		// wp_enqueue_style('index-view-style',  get_theme_file_uri( "assets/css/views/index.css" ) );
		wp_enqueue_script( 'dropdown-cards-script', get_template_directory_uri() . '/assets/js/dropdown-cards.js', '', 1.1, true );
	}


}
add_action( 'wp_enqueue_scripts', 'add_theme_scripts' );

function add_admin_theme_scripts() {

	wp_enqueue_style('fork-awesome-editor-style',  get_theme_file_uri( "lib/fork-awesome/css/fork-awesome.min.css" ) );
	wp_enqueue_style('awt-editor-style',  get_theme_file_uri( "assets/css/build/awt.css" ) );
	wp_enqueue_style('user-blocks-editor-style',  get_theme_file_uri( "assets/css/build/user-blocks.css" ) );
	wp_enqueue_style('views-editor-style',  get_theme_file_uri( "assets/css/build/views.css" ) );

	// Enqueue scripts
	wp_enqueue_script( 'swiper-script', get_template_directory_uri() . '/lib/swiper/swiper-element.min.js', '', 1.1, true );

	// wp_enqueue_script( 'swiper-script', get_template_directory_uri() . '/lib/swiper/swiper.min.js', '', 1.1, true );
	wp_enqueue_script( 'main-script', get_template_directory_uri() . '/assets/js/main.js', '', 1.1, true );
	wp_enqueue_script( 'variations-script', get_template_directory_uri() . '/variations.js', 'wp-blocks', 1.1, true );
	// wp_enqueue_script( 'mart-script', get_template_directory_uri() . '/assets/js/multiple-animated-rich-text.js', '', 1.1, true );

}
add_action('enqueue_block_editor_assets', 'add_admin_theme_scripts');

function awt_load_custom_blocks(){
	// Library blocks
	// register_block_type( __DIR__ . '/build/lib/section-bg-video');
	// register_block_type( __DIR__ . '/build/lib/rich-text');
	// register_block_type( __DIR__ . '/build/lib/animated-rich-text');
	// register_block_type( __DIR__ . '/build/lib/multiple-animated-rich-text');
	// Custom blocks
	register_block_type( __DIR__ . '/build/section-bg-video-home');
	register_block_type( __DIR__ . '/build/section-hero');
	register_block_type( __DIR__ . '/build/section-hero-post');
	register_block_type( __DIR__ . '/build/section');
	register_block_type( __DIR__ . '/build/section-carrousel');
	register_block_type( __DIR__ . '/build/section-carrousel-slide');
	register_block_type( __DIR__ . '/build/case-study-summary');
	register_block_type( __DIR__ . '/build/dropdown-card');
	register_block_type( __DIR__ . '/build/rich-text');
	register_block_type( __DIR__ . '/build/fork-awesome-icon');
	register_block_type( __DIR__ . '/build/animated-rich-text');
	register_block_type( __DIR__ . '/build/multiple-animated-rich-text');
	// register_block_type( __DIR__ . '/build/button');
}

add_action( 'init', 'awt_load_custom_blocks');

// SVG support
function add_file_types_to_uploads($file_types){
	$new_filetypes = array();
	$new_filetypes['svg'] = 'image/svg+xml';
	$file_types = array_merge($file_types, $new_filetypes );
	return $file_types;
}
add_filter('upload_mimes', 'add_file_types_to_uploads');

?>