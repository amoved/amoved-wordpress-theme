<?php

if ( ! function_exists( 'awt_theme_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which runs
 * before the init hook.
 */
function awt_theme_setup() {
	// Add support for block styles.
	add_theme_support( 'wp-block-styles' );

	/*
	 * Load additional block styles.
	 */
	$styled_blocks = ['group', 'columns', 'video', 'button', 'quote'];
	foreach ( $styled_blocks as $block_name ) {
		$args = array(
			'handle' => "awt-$block_name",
			'src'    => get_theme_file_uri( "assets/css/blocks/$block_name.css" ),
			$args['path'] = get_theme_file_path( "assets/css/blocks/$block_name.css" ),
		);
		wp_enqueue_block_style( "core/$block_name", $args );
	}
}
endif; 
add_action( 'after_setup_theme', 'awt_theme_setup' );

function add_theme_scripts() {

	// Enqueue general purpose styles
	wp_enqueue_style('swiper-style',  get_theme_file_uri( "lib/swiper/swiper-bundle.min.css" ) );
	wp_enqueue_style('style',  get_theme_file_uri( "assets/css/style.css" ) );

	// Enqueue editor styles.
	// add_editor_style( 'editor-style.css' );

	// Enqueue scripts
	wp_enqueue_script( 'swiper-script', get_template_directory_uri() . '/lib/swiper/swiper.min.js', '', 1.1, true );
	wp_enqueue_script( 'script', get_template_directory_uri() . '/assets/js/main.js', '', 1.1, true );

}
add_action( 'wp_enqueue_scripts', 'add_theme_scripts' );

?>