//-----------------------------//
//------- Browser utils -------//
//-----------------------------//

function docReady(fn) {
  // see if DOM is already available
  if (document.readyState === "complete" || document.readyState === "interactive") {
      // call on next available tick
      setTimeout(fn, 1);
  } else {
      document.addEventListener("DOMContentLoaded", fn);
  }
}

// var swiper = new Swiper(".swiper", {
//     loop: true,
//     pagination: {
//       el: ".swiper-pagination",
//       clickable: true,
//     },
//     navigation: {
//         nextEl: ".swiper-button-next",
//       },
//     autoplay: {
//         delay: 4000,
//         disableOnInteraction: true,
//     }
// });

window.onscroll = function() {scrollFunction()};

function scrollFunction() {
  if (document.body.scrollTop > 80 || document.documentElement.scrollTop > 80) {
    document.getElementsByClassName("awt-header")[0].parentElement.classList.add('scrolled');
  } else {
    document.getElementsByClassName("awt-header")[0].parentElement.classList.remove('scrolled');
  }
};