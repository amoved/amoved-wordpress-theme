import { 
    useBlockProps, 
    InnerBlocks, 
} from '@wordpress/block-editor';
import { useSelect } from '@wordpress/data';
import './editor.scss'

const ALLOWED_BLOCKS = [
    "awt/rich-text",
    "awt/multiple-animated-rich-text",
    "awt/section-carrousel",
    "core/block", 
    "core/paragraph", 
    "core/columns"
]
export default function editBlock({isSelected, clientId}) {

    const isInnerBlockSelected = useSelect(
        ( select ) => select( 'core/block-editor' ).hasSelectedInnerBlock( clientId, true )
    )

    return (
        <div { ...useBlockProps() } data-collapsed={!isSelected && !isInnerBlockSelected}>
            <div className='container'>
                <InnerBlocks allowedBlocks={ALLOWED_BLOCKS} />
            </div>
            <div className='dropdown-btn-container'>
                <button>
                    <img src="http://localhost:8000/wp-content/uploads/2023/04/flecha-boton-enlace.png" />
                </button>
            </div>
        </div>
    );
}