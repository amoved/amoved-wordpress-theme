import { useBlockProps, InnerBlocks } from '@wordpress/block-editor';
import './style.scss'

export default function saveBlock() {
    const blockProps = useBlockProps.save();
    
    return (
        <div { ...blockProps }>
            <div className='block-inner row'>
                <div className='col'>
                    <InnerBlocks.Content />
                </div>
            </div>
        </div>
    );
}
