//---------------------------//
//---- Sort description -----//
//---------------------------//
// 
// This scripts manages each loop that shows one by one the animated-rich-text blocks
// inside every multiple-animated-rich-text blocks

//-----------------------//
//--- Global configs ----//
//-----------------------//
// - containerClass: the name of the wordpress generated class of the container block
// - containerPrefix: custom prefix for container block
// - innerBlockClass: the name of the wordpress generated class of the text block
// - animationName: the animation name of the animation that, once finished, triggers 
//   the next text block
const containerClass = 'wp-block-awt-multiple-animated-rich-text'
const containerPrefix = 'awt-mart-'
const innerBlockClass = 'wp-block-awt-animated-rich-text'
const animationName = 'animated-text'

//-----------------------//
//---- Global state -----//
//-----------------------//

// Container State Interface:
// - containerEl: the html container element 
// - currentIndex: index of the visible animated-rich-text block
// - innerElsArr: the html elements
let containersState= []

//---------------------------//
//---- Global functions -----//
//---------------------------// 

// Add a new container to manage its own animation
const addContainer = (containerEl,innerEls) => {
  // Add name attribute to identify elements
  containerEl.setAttribute("name", `${containerPrefix}${containersState.length}`)
  containersState.push({
    containerEl, 
    currentIndex: 0,
    innerElsArr: innerEls
  })
}

const getContainerIndexFromEl = (el) => {
  const containerEl = el.closest(`.${containerClass}`)
  if(containerEl){
    return containerEl.getAttribute('name').substring(containerPrefix.length)
  } else {
    console.log('No container found')
    return null
  }
}

// Check if provided element is the active element in the animation
const isCurrentEl = (el) => {
  const containerIndex = getContainerIndexFromEl(el)
  // Check if element is contained
  if(containerIndex > 0 && containerIndex < containersState.length){
    return containersState[containerIndex].includes(el)
  } else {
    console.log('Cointainer id not found or out of range')
    return false
  }
}

const iterate = (el) => {
    const containerIndex = getContainerIndexFromEl(el)
    // Get previous index and then add one
    const prevElIndex = containersState[containerIndex].currentIndex++
    // Hide current element
    containersState[containerIndex].innerElsArr[prevElIndex].style.display = 'none'
    // If a loop has been completed restart index
    if( (prevElIndex+1) % containersState[containerIndex].innerElsArr.length == 0){
        containersState[containerIndex].currentIndex = 0
    }
    const newElIndex = containersState[containerIndex].currentIndex
    // Show new element
    containersState[containerIndex].innerElsArr[newElIndex].style.display = 'block'
}
    

//---------------------------------------------//
//---- Initialize and add event listeners -----//
//---------------------------------------------//

const init = (document) => {
  // Get all container blocks
  const containersElArr = Array.from(document.getElementsByClassName(containerClass))
  
  containersElArr.forEach(containerEl => {
    
    // Get all text blocks
    const animatedTextElsArr = Array.from(containerEl.getElementsByClassName(innerBlockClass))
    // Store state
    addContainer(containerEl,animatedTextElsArr)
    animatedTextElsArr.forEach((animatedTextEl,i) => {
      
      // Init, hide all text elements but the first one
      if(i!=0){
        animatedTextEl.style.display = 'none'
      }
      // Add event listener to change displayed text element 
      animatedTextEl.addEventListener('animationiteration', (event) =>{
        if (event.animationName !== animationName) {
          return;
        } else {
          iterate(event.target)
        }
      })
    })
  })
}

// Launch it for front-end 
docReady(() => {    
  init(document)
})


// Launch it for both front-end and editor view
// docReady(() => {
//   setTimeout(()=>{
//     const iframeCanvasEl = document.getElementsByClassName('edit-site-visual-editor__editor-canvas')[0]
//     console.log(iframeCanvasEl)
//     if(iframeCanvasEl){
//       console.log('We have an iframe!')
//     console.log(iframeCanvasEl.contentDocument)

//       init(iframeCanvasEl.contentDocument)
//     } else {
//       console.log('We are at the front!')
//       init(document)
//     }
//   },2000)
// })
