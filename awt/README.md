### Changes

In order to add changes to theme you have to first compile both the styles and the components. Do so by running the scripts stored in the packeage.json file.

Keep in mind that WP stores all the content of the pages created with the theme asociated to it therefore, to update components or styles you shouldn't replace the all theme directory, instead replace the css/build and build folders once you have finish. You can use commands like:

```
rsync -r awt [path to server's dir to store the theme]
ssh [server]
cd [service dir]
docker cp awt/build wordpress-amoved-wordpress-1:/var/www/html/wp-content/themes/awt/
```

### Acknwodlegments

- Square ruler icon: <a href="https://www.flaticon.com/free-icons/measure" title="measure icons">Measure icons created by DinosoftLabs - Flaticon</a>
- Pen and ruler icon: <a href="https://www.flaticon.com/free-icons/pen" title="pen icons">Pen icons created by Freepik - Flaticon</a>
- Web icon: <a href="https://www.flaticon.com/free-icons/www" title="www icons">Www icons created by Muhammad Ali - Flaticon</a>
- CMS icon: <a href="https://www.flaticon.com/free-icons/cms" title="cms icons">Cms icons created by lakonicon - Flaticon</a>
- Webapp icon: <a href="https://www.flaticon.com/free-icons/desktop" title="desktop icons">Desktop icons created by xnimrodx - Flaticon</a>
- Technical support icon: <a href="https://www.flaticon.com/free-icons/construction-and-tools" title="construction and tools icons">Construction and tools icons created by Muhammad_Usman - Flaticon</a>