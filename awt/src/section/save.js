import { useBlockProps, InnerBlocks } from '@wordpress/block-editor';
import './style.scss'

export default function saveBlock( { attributes } ) {
    const blockProps = useBlockProps.save();
    
    return (
        <section { ...blockProps }> 
            <div className="container">
                <InnerBlocks.Content />
            </div>   
        </section>
    );
}
