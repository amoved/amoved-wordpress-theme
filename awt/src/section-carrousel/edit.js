import { 
	PanelBody,
	PanelRow,
	ToggleControl
} from "@wordpress/components"
import { 
	InnerBlocks,
	InspectorControls,
	BlockControls,
	useBlockProps
} from '@wordpress/block-editor';
import { __ } from '@wordpress/i18n';
import './editor.scss'

const ALLOWED_BLOCKS = [ 'awt/section-carrousel-slide' ];

export default function editBlock( { attributes, setAttributes, isSelected } ) {
    
	const {
		loop,
		autoplay,
		navigation,
		pagination
	} = attributes

	if(isSelected) {
		return (
			<>
				<BlockControls group="other">
				{/* <ToolbarGroup>
					<ToolbarButton isPressed={props.attributes.size === "large"} onClick={() => props.setAttributes({ size: "large" })}>
						Large
					</ToolbarButton>
				</ToolbarGroup> */}

					<InspectorControls>
						<PanelBody title="Opciones de carrusel" initialOpen={true}>
							<PanelRow>
								<ToggleControl label="Bucle" checked={loop} onChange={(isChecked) => {setAttributes({loop: isChecked})}} />
								<ToggleControl label="Autoplay" checked={autoplay} onChange={(isChecked) => {setAttributes({autoplay: isChecked})}} />
							</PanelRow>
							<PanelRow>
								<ToggleControl label="Navegación" checked={navigation} onChange={(isChecked) => {setAttributes({navigation: isChecked})}} />
								<ToggleControl label="Paginación" checked={pagination} onChange={(isChecked) => {setAttributes({pagination: isChecked})}} />
							</PanelRow>
						</PanelBody>
					</InspectorControls>
				</BlockControls>
				<div {...useBlockProps()}>
					<InnerBlocks allowedBlocks={ALLOWED_BLOCKS} />
				</div>
			</>
		);
	}

	return (
		<>
			<div {...useBlockProps()}>
				<swiper-container 
					slides-per-view="1" 
					navigation={navigation} 
					pagination={pagination} 
					loop={loop} 
					autoplay={autoplay}
					scrollbar="false"
				>
					<InnerBlocks allowedBlocks={ALLOWED_BLOCKS} />
				</swiper-container>
			</div>
		</>
    );
    // return (
    //     <>
	// 		<BlockControls group="other">
	// 		</BlockControls>
    //         <div className='container row swiper' {...useBlockProps()}>
	// 			<div className='swiper-wrapper'>
	// 				<InnerBlocks allowedBlocks={ALLOWED_BLOCKS} />
	// 			</div>
	// 			<div className='swipper-button-next'></div>
	// 			<div className='swipper-button-pagination'></div>
	// 		</div>
    //     </>
    // );
}