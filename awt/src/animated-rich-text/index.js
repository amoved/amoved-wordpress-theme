import { registerBlockType } from '@wordpress/blocks'
import editBlock from './edit'
import saveBlock from './save'
import metadata from './block.json'

registerBlockType(metadata, {
    edit: editBlock,
    save: saveBlock
})