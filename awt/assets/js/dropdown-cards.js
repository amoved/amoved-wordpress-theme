
// Source:  https://css-tricks.com/using-css-transitions-auto-dimensions/
function collapse(element) {
  // Get the height of the element's inner content, regardless of its actual size
  var elHeight = element.scrollHeight;
  
  // Temporarily disable all css transitions
  var elementTransition = element.style.transition;
  element.style.transition = '';
  
  // On the next frame (as soon as the previous style change has taken effect),
  // explicitly set the element's height to its current pixel height, so we 
  // aren't transitioning out of 'auto'
  requestAnimationFrame(function() {
    element.style.height = elHeight + 'px';
    element.style.transition = elementTransition;
    
    // On the next frame (as soon as the previous style change has taken effect),
    // have the element transition to its previous height by clearing this
    requestAnimationFrame(function() {
      element.style.height = '';
    });
  });
  
}

function expand(element) {

  // Get the height of the element's inner content, regardless of its actual size
  var elHeight = element.scrollHeight;
  console.log(element);
  console.log(elHeight);
  console.log(element.style.height);
  
  // Have the element transition to the height of its inner content
  element.style.height = elHeight + 'px';

  // When the next css transition finishes (which should be the one we just triggered)
  element.addEventListener('transitionend', function(e) {
    console.log('transition eneded')
    console.log(element.style.height);

    // Remove this event listener so it only gets triggered once
    element.removeEventListener('transitionend', arguments.callee);
  });
  
}

// Launch it for front-end 
docReady(() => {   
  const cards = Array.from(document.querySelectorAll('.wp-block-awt-dropdown-card'))
  
  cards.forEach(card => {
    // Add event listener to trigger expand or collapse animations
    card.addEventListener('click', (e) => {
      const card = e.currentTarget   
      const contentContainerEl = card.querySelector('.container')    
      if(card.getAttribute('data-collapsed') === 'true') {
        expand(contentContainerEl)
        card.querySelector('.dropdown-btn-container p').innerText = "Ver menos"
        
        // Mark the section as "currently not collapsed"
        card.setAttribute('data-collapsed', 'false');
      } else {      
        collapse(contentContainerEl)
        card.querySelector('.dropdown-btn-container p').innerText = "Ver más"

        // Mark the section as "currently collapsed"
        card.setAttribute('data-collapsed', 'true');
      }
    }, true)
  })
})

