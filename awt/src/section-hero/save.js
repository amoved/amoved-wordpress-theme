import { useBlockProps, InnerBlocks } from '@wordpress/block-editor';
import './style.scss'

export default function saveBlock( { attributes } ) {
    const blockProps = useBlockProps.save();
    
    return (
        <section { ...blockProps }>
            { attributes.src && (
				<video
					autoPlay=""
					loop=""
					src={ attributes.src }
                    className="bg-video"
				/>
			) }
            <div className='container row'>
                <div className='col awt-col-12 awt-col-lg-10 awt-col-xl-8'>
                    <InnerBlocks.Content />
                </div>
            </div>
        </section>
    );
}
