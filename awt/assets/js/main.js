var swiper = new Swiper(".awt-projects-carrousel", {
    loop: true,
    pagination: {
      el: ".swiper-pagination",
      clickable: true,
    },
    navigation: {
        nextEl: ".swiper-button-next",
      },
    autoplay: {
        delay: 4000,
        disableOnInteraction: true,
    }
});

window.onscroll = function() {scrollFunction()};

function scrollFunction() {
  if (document.body.scrollTop > 80 || document.documentElement.scrollTop > 80) {
    document.getElementById("awt-header").classList.add('scrolled');
  } else {
    document.getElementById("awt-header").classList.remove('scrolled');
  }
};