import { 
    useBlockProps, 
	BlockControls,
    InnerBlocks, 
	MediaReplaceFlow
} from '@wordpress/block-editor';
import { useDispatch } from '@wordpress/data';
import { store as noticesStore } from '@wordpress/notices';
import { __ } from '@wordpress/i18n';
import './editor.scss'

const ALLOWED_MEDIA_TYPES = [ 'video', 'image' ];
const ALLOWED_BLOCKS = ["awt/rich-text","awt/multiple-animated-rich-text","core/block", "core/columns", "core/column","core/paragraph"]

export default function editBlock( { attributes, setAttributes } ) {
    
	const { id, src } = attributes;

    function onSelectMedia( media ) {
		if ( ! media || ! media.url ) {
			// In this case there was an error
			// previous attributes should be removed
			// because they may be temporary blob urls.
			setAttributes( {
				src: undefined,
				id: undefined,
				poster: undefined,
				caption: undefined,
			} );
			return;
		}

		// Sets the block's attribute and updates the edit component from the
		// selected media.
		setAttributes( {
			src: media.url,
			id: media.id,
			poster:
				media.image?.src !== media.icon ? media.image?.src : undefined,
			caption: media.caption,
		} );
	}

    const { createErrorNotice } = useDispatch( noticesStore );
	function onUploadError( message ) {
		createErrorNotice( message, { type: 'snackbar' } );
	}

    return (
        <>
            <BlockControls group="other">
				<MediaReplaceFlow
					name={ src ? __('Replace background media') : __('Add background media') }
					mediaId={ id }
					mediaURL={ src }
					allowedTypes={ ALLOWED_MEDIA_TYPES }
					// accept={["video/*","image/*"]}
					onSelect={ onSelectMedia }
					onError={ onUploadError }
				/>
			</BlockControls>
            <section { ...useBlockProps() }>
                <video 
                    // Fixed values for controls and autoplay
                    autoPlay={true}
                    loop={true}
                    src={src}
                    className='bg-video'
                />
                <div className='container row'>
					<div className='col awt-col-12'>
						<InnerBlocks allowedBlocks={ALLOWED_BLOCKS}/>
					</div>
				</div>
            </section>
        </>
    );
}