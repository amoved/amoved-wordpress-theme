wp.blocks.registerBlockVariation(
    'awt/section-carrousel',
    [
        {
            name: 'testimonials-carrousel',
            title: 'Carrusel de testimonios',
            attributes: {
                className: 'awt-testimonials-carrousel'
            },
            innerBlocks: [
                ['awt/section-carrousel-slide', {}, [
                    ['uagb/image', {url: 'http://localhost:8000/wp-content/uploads/2023/03/ico-quote.png'}],
                ]]
            ]
            
        }
    ]
)