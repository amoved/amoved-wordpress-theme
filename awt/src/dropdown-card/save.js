import { useBlockProps, InnerBlocks } from '@wordpress/block-editor';
import './style.scss'

export default function saveBlock( { attributes } ) {
    const blockProps = useBlockProps.save();

    return (
        <div { ...blockProps } data-collapsed='true'> 
            <div className="container">
                <InnerBlocks.Content />
            </div>   
            <div className='dropdown-btn-container'>
                <button>
                    <p>Ver más</p>
                    <img src="http://localhost:8000/wp-content/uploads/2023/04/flecha-boton-enlace.png" />
                </button>
            </div>
        </div>
    );
}

